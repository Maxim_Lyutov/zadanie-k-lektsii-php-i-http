<?php
header('Content-Type: image/png');

$img = @imagecreatefrompng('Certificate.PNG'); //https://yadi.sk/i/DssZN8BPid-Txw

if ($img) 
{  
   // Текст надписи
   $text = 'Вы прошли тестирование!';
   // Замена пути к шрифту на пользовательский 
   $font = 'tahoma.ttf'; // https://yadi.sk/d/F7oBnksLyVqcKg
   $black = imagecolorallocate($img, 0, 0, 0);
   
   imagettftext($img, 20, 0, 440, 440, $black, $font, $text);
   imagepng($img);
   imagedestroy($img);
}
else
{
   echo 'error';	
}

?>