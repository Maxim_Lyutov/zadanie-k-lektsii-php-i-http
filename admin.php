<?php
  
  $upLoadDir = "tests/";
  if (!is_dir($upLoadDir)){
     mkdir($upLoadDir, 0700);
  }
  
  $upLoadFile = $upLoadDir . basename($_FILES['userfile']['name']);
  
  if (move_uploaded_file($_FILES['userfile']['tmp_name'], $upLoadFile))
  {
	$ref = "Location: http://{$_SERVER['SERVER_NAME']}/list.php";   
    header($ref);
    exit;
  }
?>

<!DOCTYPE html>
<html lang="ru">
  <head>
      <meta charset="utf-8">
      <title>Загрузка теста</title>
  </head>
  <body>
     <h2>Задание: Обработка форм </h2>
     <form enctype="multipart/form-data" action="" method="POST">
         <!-- Название элемента input определяет имя в массиве $_FILES -->
         <br>
         <input name="userfile" type="file" />
         <br>
         <input type="submit" value="Загрузить">
     </form>
     <hr>

  </body>
</html>
