<?php
$fileList = glob("tests/*.json");

function getNameTest($nameFile)
{ 
  $json = file_get_contents($nameFile, FILE_USE_INCLUDE_PATH);
  $arr = json_decode($json, true);
  return $arr['name'] ;
}

function getNomerTest($nameFile)
{ 
  $json = file_get_contents($nameFile, FILE_USE_INCLUDE_PATH);
  $arr = json_decode($json, true);
  return $arr['nomer'] ;
}

?>

<!DOCTYPE html>
<html lang="ru">
  <head>
      <meta content="text/html; charset=utf-8" />
      <title>Задание №4</title>
  </head>
  <body>
     <h2>Список тестов:  </h2>
     <table border = "1" width ="50%" >
        <tr>
           <td> # </td>
           <td> Название теста </td>
           <td> Файл </td>
		   <td> Переход на тест </td>
        </tr>
<?php foreach ($fileList as $ind => $file) : ?>
        <tr>
            <td> <?=$ind+1?> </td>
            <td> <?php echo getNameTest($file) ?> </td>
            <td> <?php echo $file ?> </td>
			<td> <a href = "/test.php?idTest=<?php echo getNomerTest($file) ?> "> Начать </a></td>
         </tr>
<?php endforeach; ?>
     </table>
     <p><a href="/Admin.php"> Добавить Тест </a></p>
  </body>
</html>
