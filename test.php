<?php
//задание 4
$fileList = glob("tests/*.json");
$nameTest = null;
$msg = null;

$arr = null;
$nomerTest = null;
$notFind = true;
$arrResponse = null;
$questionText = null;

if (!empty($_POST))
{ 
   foreach ($fileList as $file)
   {
       $json = file_get_contents($file, FILE_USE_INCLUDE_PATH);
       $arr = json_decode($json, true);
              
       if (($arr['nomer']) == $_POST['nomertest'])
       {
            foreach ( $arr['questions'] as $ind => $quest ){
                foreach ( $arr['questions'] as $ind2 => $res ){
                    if ( $arr['questions'][$ind]['options'][$ind2]['indeed'] ) 
                    {  
                       $arrResponse[$arr['questions'][$ind]['id']] = $arr['questions'][$ind]['options'][$ind2]['id'];
                       $questionText[$arr['questions'][$ind]['id']] = $arr['questions'][$ind]['text'];
                    }  
                }
             }
          break;
       }
   }
   
    $countError = 0;
    unset($_POST['nomertest']);
	$myName = $_POST['myname'];
    unset($_POST['myname']);
    foreach ($_POST as $question => $answer )
    {    
        $res = ($answer == $arrResponse[$question])? '+' :'-';
        
        if($res == '-')
        {  
           ++$countError ;
        }
        
        $printRes[] = "<p><b>Вопрос:</b> $questionText[$question] <br> $res";
    }
    
    if ($countError == 0)
    {
        header('Content-Type: image/png');

        $img = @imagecreatefrompng('Certificate.PNG'); //https://yadi.sk/i/DssZN8BPid-Txw

        if ($img) 
        {  
           $text = "Поздравляем,$myName ! Вы прошли тест : ".$arr['name'];
           $font = 'tahoma.ttf'; // https://yadi.sk/d/F7oBnksLyVqcKg
           $fone = imagecolorallocate($img, 0, 0, 0);
           
           imagettftext($img, 20, 0, 300, 440, $fone, $font, $text);
           imagepng($img);
           imagedestroy($img);
        }
        else
        {
           echo 'Сертификат оформляется...';    
        }
    }
    else
    {   echo 'Ваш результат: <br>';
        foreach ($printRes as $res)
        {
            echo $res;
        }
         echo '<p><a href = "/list.php"> Список тестов </a></p>';
    }
    
   
    exit;
}

if (!empty($_GET['idTest'])) {
    $nomerTest = htmlspecialchars(($_GET['idTest']));
    
    foreach ($fileList as $file)
    {   
        $json = file_get_contents($file, FILE_USE_INCLUDE_PATH);
        $arr = json_decode($json, true);
        
        if (($arr['nomer']) == $nomerTest)
        {
           $nameTest = $arr['name'];
           $nomerTest = $arr['nomer'];
           $msg = "Вы выбрали тест № $nomerTest : '$nameTest' ";
           $notFind = false;
           break;
        }

    }
}

if ($notFind)
{
   http_response_code(404);
   echo '<h3> ОШИБКА : Cтраница не найдена! </h3>';
   echo '<p>Введите номер теста в запросе...</p>';
   exit;
}

?>

<!DOCTYPE html>
<html lang="ru">
  <head>
      <meta charset="utf-8">
      <title>Задание №4</title>
  </head>
  <body>
     <h2> Блок Тестирования </h2>
     <hr>
     <p> <?php echo $msg ?> </p>
     <form action="" method="post">
	    <p> <input required placeholder="Ваше имя..." type="text" name='myname' value =''/></p>
        <?php foreach ( $arr['questions'] as $ind => $q ): ?>
            <h3><?php echo $ind+1 .'. '.$arr['questions'][$ind]['text'] ?> </h3>
            <?php  foreach ( $arr['questions'][$ind]['options'] as $ind2 => $variant ): ?>
            <p> <input required placeholder="?" type="radio" name=<?php echo $arr['questions'][$ind]['id'].' value ="' . $arr['questions'][$ind]['options'][$ind2]['id'].'"/> '.$arr['questions'][$ind]['options'][$ind2]['text'] ?> </p>
            <?php endforeach; ?>

        <?php endforeach; ?>
        <input type="hidden" name="nomertest" value="<?= $nomerTest ?>" />
        <p><input type="submit" /></p>
    </form>
  </body>
</html>